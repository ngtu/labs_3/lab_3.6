#pragma once
#include "DateTime.h"

class Stack
{
private:
	struct Node {
		DateTime* value;
		Node* prev;

		Node(DateTime* date, Node* prev);
	};

	Node* last;
	int size;

public:
	Stack();
	Stack(DateTime* date);
	~Stack();

	void empty();
	void push(DateTime* date);
	DateTime* pop();
	
	int getSize() { return this->size; }
	bool isEmpty() { return this->last == nullptr; }
};

