﻿#include <iostream>
#include <fstream>
#include "DateTime.h"
#include "TimeStr.h"
#include "Event.h"
#include "TimeInterval.h"
#include "Stack.h"
#include "Error.h"

void runApp();

int main()
{
    try {
        runApp();
    }
    catch (Error e) {
        std::cout << "[ERROR]: " << e.getMessage() << '\n';
        return e.getCode();
    }

    return 0;
}

void runApp() {
    Stack stack = Stack();
    stack.push(new DateTime(2023, 1, 1, 12, 15));
    stack.push(new TimeStr(2023, 1, 2, 11, 55, true));
    stack.push(new Event(2023, 1, 1, 12, 15, "Event_1"));

    // Попытка достать элемент из пустого стека
    while (1) {
        std::cout << stack.pop()->toStr() << '\n';
    }
}