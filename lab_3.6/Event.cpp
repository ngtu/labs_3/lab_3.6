#include "Event.h"

Event::Event()
	: DateTime()
{
}

Event::Event(int year, int month, int day, int hour, int minute, std::string eventName)
	: DateTime(year, month, day, hour, minute)
{
	this->eventName = eventName;
}

Event::Event(const Event& event)
	: DateTime(event)
{
	this->eventName = event.eventName;
}

std::string Event::toStr()
{
	dateTime_str->clear();

	dateTime_str->append(std::to_string(year) + '.');
	if (month < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(month) + '.');
	if (day < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(day) + ' ');

	if (hour < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(hour) + ':');
	if (minute < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(minute));

	dateTime_str->append(" [");
	dateTime_str->append(!eventName.empty() ? eventName : "Untitled");
	dateTime_str->append("]");

	return *dateTime_str;
}

std::ostream& operator<<(std::ostream& os, Event& event)
{
	os << event.toStr();

	return os;
}

std::istream& operator>>(std::istream& is, Event& event)
{
	int y, m, d, h, min;
	std::string eventName;

	is >> y >> m >> d >> h >> min >> eventName;

	event.setDate(y, m, d);
	event.setTime(h, min);
	event.eventName = eventName;

	return is;
}

std::fstream& operator<<(std::fstream& os, Event& event)
{
	os <<
		std::to_string(event.year) << ' ' <<
		std::to_string(event.month) << ' ' <<
		std::to_string(event.day) << ' ' <<
		std::to_string(event.hour) << ' ' <<
		std::to_string(event.minute) << ' ' <<
		event.eventName << '\n';

	return os;
}

std::fstream& operator>>(std::fstream& is, Event& event)
{
	int y, m, d, h, min;
	std::string eventName;

	is >> y >> m >> d >> h >> min >> eventName;

	event.setDate(y, m, d);
	event.setTime(h, min);
	event.eventName = eventName;

	return is;
}
